# Processing--RISC-Vduino

#### 介绍
Processing是数位艺术软件，纯软件平台，无法直接控制硬件，而借助RISC-Vduino，可以读取各种传感器的数值，也可控制各种机电装置，可扩展用于控制智能家居，无人飞机，机器人等各种硬件实体。Processing是一种开源的编程语言和环境，它服务于有创意的设计师和艺术家，这些人希望利用某种程序，来创意静态图像和动态影像，并使创作的作品能与人产生良好的互动。

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
