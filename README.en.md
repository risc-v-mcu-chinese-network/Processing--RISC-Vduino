# Processing--RISC-Vduino

#### Description
Processing是数位艺术软件，纯软件平台，无法直接控制硬件，而借助RISC-Vduino，可以读取各种传感器的数值，也可控制各种机电装置，可扩展用于控制智能家居，无人飞机，机器人等各种硬件实体。Processing是一种开源的编程语言和环境，它服务于有创意的设计师和艺术家，这些人希望利用某种程序，来创意静态图像和动态影像，并使创作的作品能与人产生良好的互动。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
